#!/usr/bin/env python

import json
import sys

exports = json.load(sys.stdin)['Exports']

for export in exports:

    def find_export(export, name):
        return next((x for x in exports if x['Name'] == name), None)

    connection_string = find_export(export, 'InsuranceDatabaseConnectionString')['Value']
    subnets_list = find_export(export, 'InsuranceNetwork-PrivateSubnets')['Value']
    vpc_id = find_export(export, 'DatabaseSecurityGroup')['Value']

result = {
    'environment_variables': {
        'DATABASE_URL': connection_string
    },
    'vpc_config': {
        "SubnetIds": subnets_list.split(','),
        "SecurityGroupIds": [ vpc_id ]
    }
}

zappa_settings = json.load(open('zappa_settings.json'))
zappa_settings['production'] = dict(zappa_settings['production'].items() + result.items())

with open('zappa_settings.json', 'w') as outfile:
    json.dump(zappa_settings, outfile, indent=2, sort_keys=True)
