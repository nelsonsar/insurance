AWSTemplateFormatVersion: 2010-09-09
Description: Create a RDS instance

Parameters:
    NetworkStackName:
        Description: Name of an active CloudFormation stack that contains the networking resources such as the subnet and security group.
        Type: String
        MinLength: 1
        MaxLength: 255
        AllowedPattern: ^[a-zA-Z][-a-zA-Z0-9]*$
        Default: InsuranceNetwork

    DBName:
        Default: insurance
        Description: The database name
        Type: String
        MinLength: 1
        MaxLength: 64
        AllowedPattern: ^[a-zA-Z][-a-zA-Z0-9]*$

    DBUsername:
        Default: admin
        NoEcho: true
        Description: The database admin account username
        Type: String
        MinLength: 1
        MaxLength: 16
        AllowedPattern: ^[a-zA-Z][-a-zA-Z0-9]*$

    DBPassword:
        Default: notsafepassword
        NoEcho: true
        Description: The database admin account password
        Type: String
        MinLength: 8
        MaxLength: 41
        AllowedPattern: ^[a-zA-Z][-a-zA-Z0-9]*$

    DBClass:
        Default: db.t2.small
        Description: Database instance class
        Type: String
        AllowedValues:
            - db.m3.medium
            - db.m3.large
            - db.t2.micro
            - db.t2.small
            - db.t2.large
            - db.t2.xlarge
            - db.t2.2xlarge

    DBAllocatedStorage:
        Default: 5
        Description : The size of the database (Gb)
        Type: Number
        MinValue: 5
        MaxValue: 1024

Resources:
    DBSubnetGroup:
      Type: AWS::RDS::DBSubnetGroup
      Properties:
        DBSubnetGroupDescription: Subnets available for the RDS DB Instance
        SubnetIds:
            Fn::Split:
                - ','
                - Fn::ImportValue:
                    !Sub "${NetworkStackName}-PrivateSubnets"

    VPCSecurityGroup:
        Type: AWS::EC2::SecurityGroup
        Properties:
           GroupDescription: Security group for RDS DB Instance.
           SecurityGroupIngress:
               - IpProtocol: tcp
                 CidrIp:
                     Fn::ImportValue:
                         !Sub "${NetworkStackName}-PrivateSubnet1CIDR"
                 FromPort: 3306
                 ToPort: 3306
               - IpProtocol: tcp
                 CidrIp:
                     Fn::ImportValue:
                         !Sub "${NetworkStackName}-PrivateSubnet2CIDR"
                 FromPort: 3306
                 ToPort: 3306
           VpcId:
               Fn::ImportValue:
                   !Sub "${NetworkStackName}-VPCID"
    MyDB:
        Type: AWS::RDS::DBInstance
        Properties:
            DBName: !Ref DBName
            AllocatedStorage: !Ref DBAllocatedStorage
            DBInstanceClass: !Ref DBClass
            Engine: MySQL
            EngineVersion: 5.7
            MasterUsername: !Ref DBUsername
            MasterUserPassword: !Ref DBPassword
            DBSubnetGroupName: !Ref DBSubnetGroup
            VPCSecurityGroups:
                - !Ref VPCSecurityGroup

Outputs:
    ConnectionString:
        Description: MyPy connection string
        Value:
            Fn::Join:
                - ""
                - - "mysql+pymysql://"
                  - !Ref DBUsername
                  - ":"
                  - !Ref DBPassword
                  - "@"
                  - Fn::GetAtt:
                      - MyDB
                      - Endpoint.Address
                  - "/"
                  - !Ref DBName
        Export:
            Name: InsuranceDatabaseConnectionString

    SecurityGroupId:
        Description: Security Group ID for database connection
        Value: !Ref VPCSecurityGroup
        Export:
            Name: DatabaseSecurityGroup

