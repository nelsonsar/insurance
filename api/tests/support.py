import json
import unittest
from insurance.factory import create_app, db, security
from insurance.authentication.models import User
from insurance.risk.models import RiskType, RiskTypeField
from insurance.config import Config, TestingConfig

class ResourceTestCase(unittest.TestCase):
    """A base class to test resources endpoints."""

    def setUp(self):
        """Create an application instance and seeds the database.

        The setUp method creates an instance of the application using
        the configurations defined by the test runner and within the
        new application context we create the database and also create
        test records for users and risk types. Since it's not possible to
        use the models outside this context the needed information need
        to be gathered and saved in the test context (as local variables).
        """

        self.app = create_app(TestingConfig())
        self.client = self.app.test_client()
        with self.app.app_context():
            db.init_app(self.app)
            db.create_all()
            self.__seed_database(security.datastore, db)

    def user_id(self):
        """Return the user's id created in the setUp step."""

        return self._user_id

    def risk_type_id(self):
        """Return the risk type id created in the setUp step."""

        return self._risk_type_id

    def user_access_token(self):
        """Return the user's access token created in the setUp step."""
        return self._user_access_token

    def _post(self, url, data, headers={}):
        response = self.client.post(url, data=data, headers=headers)

        return TestResponse(response.status_code, response.data)

    def _get(self, url, headers={}):
        response = self.client.get(url, headers=headers)

        return TestResponse(response.status_code, response.data)


    def _invalid_credentials_payload(self):
        return dict(message="Invalid Credentials")

    def __seed_database(self, datastore, db):
        if not datastore.find_user(email='admin@admin.com'):
            risk_type = self.__create_risk_type()
            user = self.__create_user(datastore, [risk_type])
            db.session.commit()
            self._user_id = user.id
            self._risk_type_id = risk_type.id
            self._user_access_token = user.get_auth_token()

    def __create_risk_type(self):
        name = RiskTypeField(name='Name', field_type='text')
        age = RiskTypeField(name='Age', field_type='numeric')

        fields = [name, age]

        return RiskType(name='Automobile', fields=fields)

    def __create_user(self, datastore, risk_types):
        return datastore.create_user(
            email='admin@admin.com',
            password='admin',
            risk_types=risk_types
        )

class TestResponse(object):
    def __init__(self, status_code, raw_body):
        self._status_code = status_code
        self._body = json.loads(raw_body)

    @property
    def status_code(self):
        return self._status_code

    @property
    def body(self):
        return self._body
