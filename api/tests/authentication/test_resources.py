import unittest
from werkzeug.exceptions import HTTPException

from tests.support import ResourceTestCase

class TestAuthentication(ResourceTestCase):

    def _post_token_request(self, data):
        return self._post('/token', data)

    def test_post_token_fails_with_no_email(self):
        expected = { 'message': { 'email': "Invalid email given" } }

        response = self._post_token_request(dict(password='123'))

        self.assertEqual(expected, response.body)
        self.assertEqual(400, response.status_code)

    def test_post_token_fails_with_no_password(self):
        expected = { 'message': { 'password': "Invalid password given" } }

        response = self._post_token_request(dict(email='john@john.com'))

        self.assertEqual(expected, response.body)
        self.assertEqual(400, response.status_code)

    def test_post_token_fails_when_user_cant_be_found(self):
        expected = self._invalid_credentials_payload()

        response = self._post_token_request(dict(
            email='admin@admin',
            password='admin'
        ))

        self.assertEqual(expected, response.body)
        self.assertEqual(401, response.status_code)

    def test_post_token_fails_with_invalid_password(self):
        expected = self._invalid_credentials_payload()

        response = self._post_token_request(dict(
            email='admin@admin',
            password='foo'
        ))

        self.assertEqual(expected, response.body)
        self.assertEqual(401, response.status_code)

    def test_post_token_returns_access_token_with_valid_credentials(self):
        response = self._post_token_request(dict(
            email='admin@admin.com',
            password='admin'
        ))

        self.assertTrue('access_token' in response.body)
        self.assertEqual(200, response.status_code)
