import unittest

from insurance.common.database import BaseModel
from insurance.risk.models import RiskTypeField, FieldOptions

class FieldOptionsTest(unittest.TestCase):

    def test_add_option_changes_options_count(self):
        options = FieldOptions()

        options.add('Foo')

        self.assertEqual(1, len(options))


class RiskTypeFieldTest(unittest.TestCase):

    def setUp(self):
        self.risk_type_field = RiskTypeField()

    def test_type_setter_works_with_string(self):
        field_type = 'text'

        self.risk_type_field.field_type = field_type

        expected_enum_value = self.risk_type_field.field_types_enum[field_type]

        self.assertEqual(expected_enum_value, self.risk_type_field.field_type)

    def test_options_setter_works_with_list(self):
        options = FieldOptions()
        options.add('Foo')

        self.risk_type_field.options = options

        self.assertTrue(len(self.risk_type_field.options))

    def test_field_type_str_returns_string(self):
        self.risk_type_field.field_type = 0

        self.assertEqual('text', self.risk_type_field.field_type_str)

    def test_options_list_returns_an_empty_list_when_options_is_not_defined(self):
        self.assertEqual([], self.risk_type_field.options_list)

    def test_options_list_returns_a_list_of_the_text_representation(self):
        self.risk_type_field.options = '[{"key": 0, "value": "Foo"}]'

        expected_output = [dict(key=0, value="Foo")]

        self.assertEqual(expected_output, self.risk_type_field.options_list)
