import json
import unittest
from werkzeug.exceptions import HTTPException
from flask_restful import url_for

from tests.support import ResourceTestCase
from insurance.risk.resources import RiskTypeItem

class TestAuthentication(ResourceTestCase):

    def _get_risk_type(self, url):
        return self._get(url, headers={'Authorization': self.user_access_token()})

    def test_get_risk_type_by_id_fails_without_authorization_header(self):
        with self.app.test_request_context():
            expected = self._invalid_credentials_payload()

            url = url_for('risktypeitem', user_id=self.user_id(), id=self.risk_type_id())
            response = self.client.get(url)

            self.assertEqual(401, response.status_code)
            self.assertEqual(expected, json.loads(response.data))

    def test_get_risk_type_by_id_returns_risk_type_with_valid_token(self):
        fields = set(['id', 'name', 'fields', 'created_at', 'updated_at'])

        with self.app.test_request_context():
            url = url_for('risktypeitem', user_id=self.user_id(), id=self.risk_type_id())
            response = self._get_risk_type(url)

            self.assertEqual(200, response.status_code)
            self.assertEqual(fields, set(response.body.keys()))

    def test_get_risk_type_by_id_fails_when_user_is_not_the_owner(self):
        with self.app.test_request_context():
            url = url_for('risktypeitem', user_id=999, id=self.risk_type_id())
            response = self._get_risk_type(url)

            self.assertEqual(403, response.status_code)

    def test_get_all_risk_types_fails_without_authorization_header(self):
        with self.app.test_request_context():
            expected = self._invalid_credentials_payload()

            url = url_for('risktypelist', user_id=self.user_id(), id=self.risk_type_id())
            response = self.client.get(url)

            self.assertEqual(401, response.status_code)
            self.assertEqual(expected, json.loads(response.data))

    def test_get_all_risk_types_returns_risk_type_with_valid_token(self):
        with self.app.test_request_context():
            url = url_for('risktypelist', user_id=self.user_id(), id=self.risk_type_id())
            response = self._get_risk_type(url)

            self.assertEqual(200, response.status_code)
            self.assertTrue('risk_types' in response.body.keys())
            self.assertTrue(1 == len(response.body.values()))

    def test_get_all_risk_types_fails_when_user_is_not_the_owner(self):
        with self.app.test_request_context():
            url = url_for('risktypelist', user_id=999, id=self.risk_type_id())
            response = self._get_risk_type(url)

            self.assertEqual(403, response.status_code)
