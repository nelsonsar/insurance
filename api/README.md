Serverless Insurance API
========================

This repo is meant to demonstrate how to setup a **serverless** web application using [Flask](http://flask.pocoo.org/) and [Zappa](https://github.com/Miserlou/Zappa). The code here contains a simple API to serve risk types using token authentication. You can found more about the endpoints accessing `http://localhost:5000/api/spec.html#!/spec` after the application setup.

[Click here](http://insurance-frontend-test.s3-website-sa-east-1.amazonaws.com) to see a demo of the deployed code (deployed using Zappa).

Python Version
--------------
This project was tested using Python version 2.7.

Quickstart
----------

**Step 1:** Clone the repo and install requirements (you probably want to do this inside of a [virtual environment](http://docs.python-guide.org/en/latest/dev/virtualenvs/) with a name like *venv*):

```
$ git clone git@gitlab.com:nelsonsar/insurance.git
$ cd insurance
$ pip install -r requirements.txt
```

**Step 2:** Setup the local database

This repo uses flask-Migrate to handle database migrations. The following commands will setup the initial database:

```
$ flask db init     # this will add a migrations folder to your application
$ flask db upgrade  # apply initial migrations to the database
```

See the [flask-Migrate documentation](https://flask-migrate.readthedocs.io/en/latest/) for more details information on this step.

**Step 3:** Run the application on a local server:

```
$ export FLASK_APP=insurance/development.py
$ flask run
```

Then go to [http://localhost:5000/](http://localhost:5000/) in your browser to test out the application running locally.

**Step 4:** Run tests:

You can run tests directly with [nose](http://nose.readthedocs.io):

```
$ nosetests
```


**Step 5:** Deploy to AWS using Zappa:

**Before deploying, please make sure that you have [AWS cli tool](https://docs.aws.amazon.com/cli/latest/userguide/installing.html) [configured a proper profile](https://docs.aws.amazon.com/cli/latest/userguide/cli-multiple-profiles.html) in the aws cli tool**

You are now ready to deploy! To do so, simply run the command:

```
$ cp deploy/zappa_settings.json .
$ make deploy AWS_PROFILE="your_named_profile"
```

The first step is needed because the connection string used by Zappa is supplied after the creation of the Database stack in CloudFormation.

This will create 3 different stacks: network, db and api-production. The first one configures private subnets to allow Lambda access to the database (created in the db stack as MySQL 5.7 instance). The database is populated with a default user and risk type and, the credentials are:

email: `admin@admin.com`
password: `admin`

When the process finishes, it will give you the URL of your newly deployed application!


Making Changes
--------------
If you wish to deploy new changes to your repo simply update the code and then run:

```
$ zappa update
```

Undeploy
--------
Undeploying your project is simple. Just run the following command:
```
$ zappa undeploy
```

TODO
----
* Change to a faster way to generate token - (https://github.com/mattupstate/flask-security/issues/731)
* Create a way to export API url to the frontend stack
* Change database connection string to a safer option
