import os

from flask import Flask, jsonify
from flask_restful import Api
from flask_restful_swagger import swagger

from extensions import db, migrate, security

from insurance.common import responses
from insurance.authentication.models import user_datastore
from insurance.authentication.resources import Token
from insurance.risk.resources import RiskTypeItem, RiskTypeList


def create_app(config):
    """ Create an instance of a app using the chosen configuration.

    In order to make tests easier, this method creates an instance
    of a Flask application using the configurations defined in an
    environment and initializing all needed extensions.

    The Unauthorized handler from Flask Security is overriden so,
    we can return only JSON responses even when using third party
    extensions
    """

    app = Flask(__name__)
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config.from_object(config)
    db.init_app(app)
    migrate.init_app(app, db)

    app.security = security.init_app(app, user_datastore)
    app.security.unauthorized_handler(invalid_token_handler)

    register_api(app)

    return app

def register_api(app):
    """ Map all Flask Restful resources to their routes."""

    api = swagger.docs(
        Api(),
        apiVersion='0.1',
        basePath='http://localhost:5000',
        produces=["application/json"],
        description='API to get risk types'
    )
    api.add_resource(Token, '/token')
    api.add_resource(RiskTypeItem, '/users/<int:user_id>/risk_types/<int:id>')
    api.add_resource(RiskTypeList, '/users/<int:user_id>/risk_types')

    api.init_app(app)


def invalid_token_handler():
    """ Return a dict with a message and the Unauthorized HTTP status code"""
    return dict(message=responses.unauthorized()), 401
