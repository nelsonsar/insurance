from factory import create_app, db
from flask_cors import CORS
from flask_migrate import init, migrate, upgrade, downgrade
from insurance.common.support import db_seed
from config import Config, ProductionConfig

app = create_app(ProductionConfig())
CORS(app)

def db_upgrade():
    with app.app_context():
        upgrade()

def db_downgrade():
    with app.app_context():
        downgrade()

def db_init():
    with app.app_context():
        init()

def execute_db_seed():
    db_seed(app, db)
