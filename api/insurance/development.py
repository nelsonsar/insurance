from factory import create_app, db
from flask_cors import CORS
from flask_migrate import upgrade
from insurance.common.support import db_seed
from config import Config, DevelopmentConfig

app = create_app(DevelopmentConfig())
CORS(app)

def db_upgrade():
    with app.app_context():
        upgrade()

def execute_db_seed():
    db_seed(app, db)
