import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'this-really-needs-to-be-changed'
    SECURITY_PASSWORD_SALT = 'some-salted-string'
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    SECURITY_TOKEN_AUTHENTICATION_HEADER = 'Authorization'
    WTF_CSRF_ENABLED = False
    SECURITY_HASHING_SCHEMES = ['pbkdf2_sha512']
    SECURITY_DEPRECATED_HASHING_SCHEMES = []


class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    SECURITY_HASHING_SCHEMES = ['plaintext']
    SECURITY_DEPRECATED_HASHING_SCHEMES = []
    SERVER_NAME = 'localhost'
