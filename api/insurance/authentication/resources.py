from flask_restful import abort, marshal_with, reqparse, Resource
from flask_security.utils import hash_password, verify_password
from flask_restful_swagger import swagger

from insurance.authentication.models import User
from insurance.authentication.responses import UserToken
from insurance.common import responses

post_parser = reqparse.RequestParser()
post_parser.add_argument(
    'email', dest='email',
    location='form', required=True,
    help='Invalid email given'
)
post_parser.add_argument(
    'password', dest='password',
    location='form', required=True,
    help='Invalid password given'
)

class Token(Resource):
    """Defines all methods supported by the Token resource"""


    @swagger.operation(
        notes="Get an access token for given user",
        nickname="getToken",
        responseClass=UserToken.__name__,
        parameters=[
            {
                "name": "email",
                "description": "User's email",
                "required": True,
                "allowMultiple": False,
                "dataType": 'string',
                "paramType": "form"
            },
            {
                "name": "password",
                "description": "User's password",
                "required": True,
                "allowMultiple": False,
                "dataType": 'string',
                "paramType": "form"
            }
        ],
        responseMessages=[
            {
                "code": 401,
                "message": "Invalid Credentials"
            }
        ]
    )
    @marshal_with(UserToken.resource_fields)
    def post(self):
        """Return a UserToken to user when credentials are valid.

        In order to use protected resources the user must first obtain
        an access token. This token is returned alongside with the user's
        id if the given password and email are valid in the application
        context.

        If the email is not registered in the database or the given password
        is invalid an "Unauthorized" response will be returned instead.
        """

        args = post_parser.parse_args()
        user = User.query.filter_by(email=args.email).first()
        hashed_password = hash_password(args.password)

        print args.email

        if user is None: self.__abort_with_unauthorized()

        if (verify_password(user.password, hashed_password)):
            return UserToken(user.get_auth_token(), user.id)
        else:
            self.__abort_with_unauthorized()

    def __abort_with_unauthorized(self):
        abort(401, message=responses.unauthorized())

