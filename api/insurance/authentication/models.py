from flask_security import UserMixin, RoleMixin, SQLAlchemyUserDatastore

from insurance.extensions import db
from insurance.common.database import BaseModel

roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(),
                                 db.ForeignKey('users.id')),
                       db.Column('role_id', db.Integer(),
                                 db.ForeignKey('roles.id')))


class Role(BaseModel, RoleMixin):
    __tablename__ = 'roles'

    name = db.Column(db.String(80), nullable=False, unique=True)
    description = db.Column(db.String(255))

    """Role model needed by Flask Security extension.

    This model is not used by the application and it's just created for
    the sake of the extension.

    :param name: A string, the role's name
    """
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Role %r>' % self.name


class User(BaseModel, UserMixin):
    """Represents an User in the database.

    Although user is limited in the current context of the application,
    most fields here are important to implement token based authentication
    and support multi device authentication.
    """
    __tablename__ = 'users'

    email = db.Column(db.String(255), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False)
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    last_login_at = db.Column(db.DateTime())
    current_login_at = db.Column(db.DateTime())
    last_login_ip = db.Column(db.String(45))
    current_login_ip = db.Column(db.String(45))
    login_count = db.Column(db.Integer)
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    risk_types = db.relationship('RiskType', lazy='dynamic')

    def __repr__(self):
        return '<User %r>' % self.email

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
