from insurance.extensions import db

class BaseModel(db.Model):
    """An abstract model that contains common fields for all database models"""

    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, server_default=db.func.current_timestamp())
    modified_at = db.Column(db.DateTime, server_default=db.func.current_timestamp(),
                            onupdate=db.func.current_timestamp())
