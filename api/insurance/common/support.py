from insurance.authentication.models import User
from insurance.risk.models import RiskTypeField, RiskType, FieldOptions

def db_seed(app, db):
    datastore = app.security.datastore
    with app.app_context():
        if not datastore.find_user(email='admin@admin.com'):
            risk_type = create_risk_type()
            user = create_user(datastore, [risk_type])
            db.session.commit()

def create_risk_type():
    name = RiskTypeField(name='Name', field_type='text')
    age = RiskTypeField(name='Age', field_type='numeric')
    start_date = RiskTypeField(name='Start Date', field_type='date')
    gender_options = FieldOptions().add('Male').add('Female')
    gender = RiskTypeField(
        name='Gender',
        field_type='enum',
        options=gender_options
    )

    fields = [name, age, gender, start_date]

    return RiskType(name='Automobile', fields=fields)

def create_user(datastore, risk_types):
    return datastore.create_user(
        email='admin@admin.com',
        password='admin',
        risk_types=risk_types
    )

