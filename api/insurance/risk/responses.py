from flask_restful import fields
from flask_restful_swagger import swagger

@swagger.model
class RiskTypeFieldResponse(object):
    """Representation of a risk type field."""

    options_fields = {
        'value': fields.String,
        'key': fields.Integer
    }

    resource_fields = {
        'name': fields.String,
        'field_type': fields.String(attribute=lambda x: x.field_type_str),
        'options': fields.List(fields.Nested(options_fields), attribute=lambda x: x.options_list),
        'created_at': fields.DateTime,
        'updated_at': fields.DateTime
    }

    optional = ['options']
    required = list(set(resource_fields.keys()) - set(optional))

    swagger_metadata = {
        'field_type': {
            'enum': ['text', 'numeric', 'date', 'enum']
        }
    }


@swagger.model
@swagger.nested(fields=RiskTypeFieldResponse.__name__)
class RiskTypeResponse(object):
    """Representation of a risk type resource."""

    resource_fields = {
        'id': fields.Integer,
        'name': fields.String,
        'fields': fields.List(fields.Nested(RiskTypeFieldResponse.resource_fields)),
        'created_at': fields.DateTime,
        'updated_at': fields.DateTime
    }

    required = list(resource_fields.keys())

@swagger.model
@swagger.nested(risk_types=RiskTypeResponse.__name__)
class RiskTypeListResponse(object):
    """Representation of a risk type list."""

    resource_fields = {
        'risk_types': fields.List(fields.Nested(RiskTypeResponse.resource_fields))
    }

    required = ['risk_types']
