import json
from insurance.extensions import db
from insurance.common.database import BaseModel

class RiskType(BaseModel):
    """Representation of an user defined risk type.

    Each user my define it own type of risk like automobile or houses and,
    attach fields to collect any data relevant to it.
    """

    __tablename__ = 'risk_types'
    __table_args__ = tuple(db.UniqueConstraint('name', 'user_id', name='risk_type_name_per_user_uniq'))

    name = db.Column(db.String(80), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'),
        nullable=False)
    fields = db.relationship('RiskTypeField', lazy='dynamic')

    def __repr__(self):
        return '<RiskType %r>' % self.name


class RiskTypeField(BaseModel):
    """Representation of a risk type field.

    To support custom definition of risk types it's necessary to support
    different kind of data types. This model represents a field attached to
    a parent risk type, which type of data it'll contain and, in case of the
    enum, which options the user may choose.

    The supported types are: text, numeric, date and enum.
    """

    __tablename__ = 'risk_type_fields'
    __table_args__ = tuple(db.UniqueConstraint('name', 'risk_type_id', name='risk_type_field_name_per_type_uniq'))

    name = db.Column(db.String(80), nullable=False)
    _field_type = db.Column(db.Integer, nullable=False)
    _options = db.Column(db.Text, nullable=True)
    risk_type_id = db.Column(db.Integer, db.ForeignKey('risk_types.id'),
        nullable=False)

    field_types_enum = {
        'text': 0,
        'numeric': 1,
        'date': 2,
        'enum': 3
    }

    def __repr__(self):
        return '<RiskTypeField %r>' % self.name

    @property
    def field_type(self):
        return self._field_type

    @field_type.setter
    def field_type(self, value):
        """Set the field type by a given string.

        There's no need to store strings in the database to represent
        enum values but, it's easier to handle with them using text instead
        of numbers in the code. So this setter makes possible using text
        instead of numbers to set the field type. If the given string is
        not a supported type an Exception is raised.

        :param value: A string or an integer, the type of the field.
        """

        if isinstance(value, str):
            valid_values = list(self.field_types_enum.keys())
            if not value in valid_values: raise 'Invalid Field Type'

            self._field_type = self.field_types_enum.get(value)
        else:
            self._field_type = value


    @property
    def options(self):
        """Return the JSON representation of the field options"""

        return self._options

    @options.setter
    def options(self, value):
        """Converts a iterable value to a JSON representation"""

        if not isinstance(value, str):
            options = list(tuple(value))
            self._options = json.dumps(options)
        elif isinstance(value, str):
            self._options = value


    @property
    def field_type_str(self):
        """Helper function to get the text representation of the type

        This method will get the number saved in the database and find
        in the enum types the text representation of it.
        """

        for key, value in self.field_types_enum.iteritems():
            if value == self._field_type: return key

    @property
    def options_list(self):
        """Convert the JSON representation into a list representation"""

        if self.options is None: return []

        return json.loads(self.options)

class FieldOptions(object):
    """A Value Object to make options for the enum type creation easier"""

    def __init__(self):
        self.__options = []

    def add(self, name):
        """Add a new option to the list for the enum field type"""

        key = len(self.__options)
        self.__options.append(dict(key=key, value=name))

        return self

    def __len__(self):
        return len(self.__options)

    def __iter__(self):
        return self.__options.__iter__()
