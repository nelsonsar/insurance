from flask import current_app
from flask_restful import abort, marshal_with, reqparse, Resource, fields
from flask_security import auth_token_required, current_user
from flask_restful_swagger import swagger

from insurance.common import responses
from insurance.risk.responses import RiskTypeResponse, RiskTypeFieldResponse, RiskTypeListResponse
from insurance.risk.models import RiskType, RiskTypeField

class RiskTypeList(Resource):
    """Defines all methods supported by the risk types collection resource"""

    @swagger.operation(
        notes="Get all user's risk types",
        nickname="getAll",
        responseClass=RiskTypeListResponse.__name__,
        parameters=[
            {
                "name": "Authorization",
                "description": "User's access token",
                "required": True,
                "allowMultiple": False,
                "dataType": 'string',
                "paramType": "header"
            }
        ],
        responseMessages=[
            {
                "code": 403,
                "message": "Forbidden"
            },
            {
                "code": 401,
                "message": "Invalid Credentials"
            }
        ]
    )
    @auth_token_required
    @marshal_with(RiskTypeListResponse.resource_fields)
    def get(self, user_id):
        """Return a representation of a risk type list.

        If the token owner is not the owner of this resources a
        "Forbidden" HTTP response will be returned.
        """
        if user_id != current_user.id: abort(403, message='Forbidden')

        types = current_user.risk_types.all()

        return { 'risk_types': types }


class RiskTypeItem(Resource):
    """Defines all methods supported by an individual risk type resource"""

    @swagger.operation(
        notes="Get a risk type by ID",
        nickname="get",
        responseClass=RiskTypeResponse.__name__,
        parameters=[
            {
                "name": "Authorization",
                "description": "User's access token",
                "required": True,
                "allowMultiple": False,
                "dataType": 'string',
                "paramType": "header"
            }
        ],
        responseMessages=[
            {
                "code": 403,
                "message": "Forbidden"
            },
            {
                "code": 401,
                "message": "Invalid Credentials"
            }
        ]
    )
    @auth_token_required
    @marshal_with(RiskTypeResponse.resource_fields)
    def get(self, user_id, id):
        """Return the risk type identified by the given id.

        If the resource does not exist a "Not Found" response will be returned
        and in case of the token owner is not the owner of this resource a
        "Forbidden" HTTP response will be returned.
        """
        if user_id != current_user.id: abort(403, message='Forbidden')

        return self.__risk_type_or_abort_with_not_found(id)


    def __risk_type_or_abort_with_not_found(self, id):
        risk_type = current_user.risk_types.filter(RiskType.id == id).one()

        if risk_type is None: abort(404, message='Risk Type not found')

        return risk_type

