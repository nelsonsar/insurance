Insurance Manager
=================

This is a full stack application orchestrated by AWS CloudFormation. There are two main components:

* app: A Vue.js SPA using Semantic UI ([Setup instructions](https://gitlab.com/nelsonsar/insurance/blob/master/app/README.md))
* api: A Flask API using Zappa to deploy it as a Lambda function ([Setup instructions](https://gitlab.com/nelsonsar/insurance/blob/master/api/README.md))

The live demo can be see [here](http://insurance-frontend-test.s3-website-sa-east-1.amazonaws.com) and the credentials to access it are:

* email: `admin@admin.com`
* password: `admin`
