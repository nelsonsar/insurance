import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/pages/Home'
import Login from '@/components/pages/Login'

import store from '../store/store'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      component: Login,
      props: route => ({store: store}),
      beforeEnter: (to, from, next) => {
        if (store.isAuthenticated()) {
          next('/home')
        } else {
          next()
        }
      }
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      props: route => ({store: store}),
      beforeEnter: (to, from, next) => {
        if (store.isAuthenticated()) {
          next()
        } else {
          next('/')
        }
      }
    }
  ]
})
