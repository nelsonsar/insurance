import Http from './http'

export const getRiskType = (userToken, riskTypeId) => {
  return Http.get(
    `/users/${userToken.userId}/risk_types/${riskTypeId}`,
    {
      headers: {
        'Authorization': userToken.accessToken
      }
    }
  )
}

export const getAllRiskTypes = (userToken) => {
  return Http.get(
    `/users/${userToken.userId}/risk_types`,
    {
      headers: {
        'Authorization': userToken.accessToken
      }
    }
  )
}
