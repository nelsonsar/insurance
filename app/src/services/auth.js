import Http from './http'

export const login = (credentials) => {
  return Http.post('/token', credentials, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
}
