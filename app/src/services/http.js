import axios from 'axios'

var client = axios.create({
  baseURL: process.env.API_URL,
  timeout: 3000
})

export default client
