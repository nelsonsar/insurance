const loadFromSessionStorage = () => {
  if (sessionStorage.getItem('insurance_token')) {
    return {
      accessToken: sessionStorage.getItem('insurance_token'),
      userId: sessionStorage.getItem('user_id')
    }
  }

  return {}
}

var store = {
  state: {
    loggedIn: false,
    loading: false,
    userToken: {}
  },
  isAuthenticated () {
    if (this.state.loggedIn) {
      return true
    } else if (sessionStorage.getItem('insurance_token')) {
      return true
    }

    return false
  },
  storeCredentials (userToken) {
    this.state.userToken = {
      accessToken: userToken.access_token,
      userId: userToken.user_id
    }
    this.state.loggedIn = true
    sessionStorage.setItem('insurance_token', userToken.access_token)
    sessionStorage.setItem('user_id', userToken.user_id)
  },
  userToken () {
    if (!this.state.userToken.userId) {
      this.state.userToken = loadFromSessionStorage()
    }

    return this.state.userToken
  }
}

export default store
